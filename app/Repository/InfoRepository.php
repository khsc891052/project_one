<?php 

namespace App\Repository;

use App\Models\Info;
use App\Models\InfoProcessed;

class InfoRepository
{

    /**
     * 插入資料到database
     */
    public function insertInfo($dataset)
    {   
        Info::updateOrCreate(array('_id' => $dataset['_id']), $dataset);
    }

    public function insertInfoproccessed($dataset)
    {
        InfoProcessed::updateOrCreate(array('id' => $dataset['id']), $dataset);
    }

}
