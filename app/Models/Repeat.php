<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Repeat extends Model
{
    protected  $table = 'repeat';
    public $primaryKey = 'serial';
    public $timestamps = false;

}
