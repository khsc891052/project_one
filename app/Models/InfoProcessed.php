<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoProcessed extends Model
{
    protected  $table = 'info_processed';
    public $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

}
