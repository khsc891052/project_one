<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected  $table = 'Info';
    protected $primaryKey = '_id';

    protected $guarded  = [];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['_id'];
    }
}
