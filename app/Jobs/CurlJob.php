<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\CurlService;

class CurlJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $curl;

    /**
     * Create a new job instance.
     *  $flag false => url , true => curl_data 
     * @return void
     */
    public function __construct($curl)
    {
        //
        $this->curl = $curl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $curl_service = new CurlService();
        
        $curl_data = $curl_service->getInfo($this->curl);
        $curl_service->insertCurldata($curl_data);

    }

      /**
     * 處理一個失敗的任務
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
        
    }

}
