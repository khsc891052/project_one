<?php 
    
namespace App\Services;

use App\Repository\InfoRepository;
use App\Jobs\CurlJob;

class CurlService
{
    //curl 網站並轉出array
    public function getInfo($url)
    {
        $curl = new \Curl\Curl();
        $curl->setOpt(CURLOPT_RETURNTRANSFER, true);
        $curl->get($url);
        $curl_data = $curl->response;

        $curl->close();

        return json_decode($curl_data, true);
    }

    /**
     *  處理需存取的資料
     */
    public function processData($data){

        $dataset2 = $data;
        $dataset2['index'] = $data['_index'];
        $dataset2['type'] = $data['_type'];
        $dataset2['id'] = $data['_id'];
        $dataset2['score'] = $data['_score'];
        $dataset2['timestamp'] = substr($data['@timestamp'], 0, 19);
        
        unset($dataset2['_index'], $dataset2['_type'], $dataset2['_id'], $dataset2['_score'], $dataset2['@timestamp']);

        return $dataset2;
    }

    /**
     * 把curl下來的資料寫入資料庫
     */
    public function insertCurldata($data){

        $info_repository = new InfoRepository();

        foreach ($data['hits']['hits'] as $value) { 

            $dataset = array();

            //把每筆欄位資料存入$dataset陣列中
            foreach ($value as $data_key => $data_value) {

                if ($data_key == 'sort') {
                    $dataset[$data_key] = $data_value[0];
                    continue;
                }

                if (is_array($data_value)) {
                    foreach ($data_value as $array_key => $array_value) {
                        $dataset[$array_key] = $array_value;
                    }
                } else {
                    $dataset[$data_key] = $data_value;
                }
            }

            $info_repository->insertInfo($dataset);
            $info_repository->insertInfoproccessed($this->processData($dataset));
        }
    }

    /**
     *  回傳需要幾次一萬筆資料
     */
     public function getTimes($time_start, $time_end){

        $data = $this->getInfo("http://train.rd6/?start=" . $time_start . "&end=" . $time_end . "&from=0");

        return (int)($data['hits']['total'] / 10000);

     }

     
     public function insertAllhits($time_start, $time_end){

        $count = $this->getTimes($time_start, $time_end);

        for ($i = 0; $i <= $count; $i++) {

            $job = new CurlJob("http://train.rd6/?start=" . $time_start . "&end=" . $time_end . "&from=" . $i * 10000);
            dispatch($job);

        }
     }

}