<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\CurlService;
use App\Jobs\CurlJob;
use Faker\Provider\zh_CN\DateTime;

class CurlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'curl:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'curl and save database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get now time
        $now = new \DateTime('Asia/Taipei');
        $curl_service = new CurlService();
        $now->modify('-1 minute');

        $curl_service->insertAllhits($now->format('Y-m-d\TH:i:00'), $now->format('Y-m-d\TH:i:29'));
        $curl_service->insertAllhits($now->format('Y-m-d\TH:i:30'), $now->format('Y-m-d\TH:i:59'));

    }
    
}
